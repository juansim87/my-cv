import { useState } from "react";
import "./App.scss";
import PersonalInfo from "./components/PersonalInfo/PersonalInfo";
import About from "./components/About/About";
import Experience from "./components/Experience/Experience";
import Education from "./components/Education/Education";
import More from "./components/More/More";
import { CV } from "./CV/CV";

const {
  personalInfo,
  about,
  education,
  experience,
  languages,
  abilities,
  volunteer,
} = CV;

const App = () => {
  
  const [showEducation, setShowEducation] = useState(true);
  return (
    <div className="App">
     
      <PersonalInfo personalInfo={personalInfo} />
      <About about={about} />
      <div className="career-nav">
      <button className="career-btn" onClick={() => setShowEducation(true)}>
        Formación Académica
      </button>
      <button className="career-btn" onClick={() => setShowEducation(false)}>
        Trayectoria Profesional
      </button>
      </div>
      <div className="career-info">
        {showEducation ? (
          <Education education={education} />
        ) : (
          <Experience experience={experience} />
        )}
      </div>
      <More languages={languages} abilities={abilities} volunteer={volunteer} />
    </div>
  );
};

export default App;
