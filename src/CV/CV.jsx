export const CV = {
  personalInfo: {
    name: "Juan",
    surnames: "Simón Abad",
    city: "Madrid",
    email: "juansim87@gmail.com",
    birthDate: "24/10/1987",
    phone: "606060606",
    image: "https://i.imgur.com/lfDidqU.jpg",
    gitHub: "https://github.com/juansim87",
    gitLab: "https://gitlab.com/juansim87",
  },
  about: [
    {
      info: "Iniciado en el mundo de la programación con ganas de aprender los secretos del mundo digital",
    },
    {
      info: "Gestión y motivación de grupos, con experiencia en medios de comunicación",
    },
    {
      info: "Enamorado del teatro y de la aplicación de las habilidades que te otorga fuera del escenario",
    },
  ],
  education: [
    {
      name: "Licenciatura en Historia del Arte",
      date: "2005-2009",
      where: "Universidad de Salamanca",
      description: "Licenciado en Historia del Arte Antiguo, Medieval",
    },
    {
      name: "Licenciatura en Comunicación Audiovisual",
      date: "2009-2011",
      where: "Universidad de Salamanca",
      description: "Licenciado en Comunicación Audiovisual: radio, televisión, periodismo e internet",
    },
    {
      name: "Postgrado Experto en Locución Audiovisual",
      date: "2012",
      where: "Universidad Pontificia de Salamanca",
      description: "Experto en locución para comunicación, periodismo, publicidad y doblaje",
    },
    {
      name: "Máster en Periodismo y Comunicación Digital",
      date: "2016-2017",
      where: "EAE Business School",
      description: "Estrategias de comunicación y periodismo orientadas al mundo digital"
    },
    {
      name: "Bootcamp Front Developer",
      date: "2022",
      where: "UpgradeHub",
      description: "HTML, CSS, Javascript, Angular y React"
    },
  ],
  experience: [
    {
      name: "Director de La Máscara Teatro",
      date: "15/09/2012 – 30/06/2014",
      where: "Universidad Pontificia de Salamanca",
      description:
        "Dirección del grupo teatral de la Universidad Pontificia de Salamanca",
    },
    {
      name: "Operario de máquinas y traducción",
      date: "01/07/2014 – 23/12/2014",
      where: "Hergon Metropolitan",
      description:
        "Operario de maquinaria pesada y traductor inglés-español de equipos de trabajo en la obra del metro de Londres Crossrails.",
    },
    {
      name: "Jefe de redacción y presentador de noticias",
      date: "01/07/2015 – 30/07/2016",
      where: "Toigo Radio",
      description:
        "Dirección del diario radiofónico Informa2, redactor web, coordinador de RR.SS. y apoyo en labores de dirección del medio radiofónico.",
    },
    {
      name: "Técnico de Marketing de Contenidos",
      date: "28/06/2017 – 20/01/2020",
      where: "Atresmedia Corporación",
      description:
        "Promoción interna y externa de programas del grupo de comunicación, minutaje y seguimiento de programas y análisis de impacto de promociones.",
    },
    {
      name: "Redactor SEO",
      date: "21/01/2020 – Actualidad",
      where: "Atresmedia Corporación",
      description:
        "Redactor de posicionamiento en laSexta.com, apoyo en redacción de eventos informativos, estrategias de posicionamiento y mejora interna de la web.",
    },
  ],
  languages: [
    {
      language: "Español",
      wrlevel: "Nativo",
      splevel: "Nativo",
    },{
      language: "Inglés",
      wrlevel: "C1",
      splevel: "C1",
    },
  ],
  abilities: [
    "Exposición oral",
    "Redacción",
    "SEO",
    "Gestión de grupos",
  ],
  volunteer: [
    {
      name: "Centro de Acogida Padre Damián",
      where: "Cáritas Salamanca",
      description:
        "Apoyo en actividades del Centro Padre Damián para los internos, editor del programa 'Cuaderno de derrota'.",
    },
  ],
};
