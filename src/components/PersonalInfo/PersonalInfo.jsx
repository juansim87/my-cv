import React from "react";
import "./PersonalInfo.scss";

const PersonalInfo = ({ personalInfo }) => {
  return (
    <div className="personalInfo">
      
      <div className="main">
      <img className="photo" src={personalInfo.image} alt="foto" />
        <h2 className="name">
          {personalInfo.name} {personalInfo.surnames}
        </h2>
        <h3 className="job">Front End Developer</h3>
      </div>
      <ul className="card">
        <li>🏠 {personalInfo.city}</li>
        <li>📅 {personalInfo.birthDate}</li>
        <li>
          📧 
          <a href={"mailto:" + personalInfo.email}> juansim87@gmail.com</a>
        </li>
        <li>📱 {personalInfo.phone}</li>
        <div className="gitbox">
          <img
            className="gitlogo"
            src="https://i.imgur.com/jJqHqOQ.png"
            alt="github"
          />
          <a href={personalInfo.gitHub}>GitHub</a>
        </div>
        <div className="gitbox">
          <img
            className="gitlogo"
            src="https://i.imgur.com/yvsJJJF.png"
            alt="github"
          />
          <a href={personalInfo.gitLab}>GitLab</a>
        </div>
      </ul>
    </div>
  );
};

export default PersonalInfo;
