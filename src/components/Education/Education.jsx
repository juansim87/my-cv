import React from "react";
import "./Education.scss";

const Education = ({ education }) => {
  return (
    <div className="education">
      {education.map((discipline) => {
        return (
          <ul lassName="info" key={JSON.stringify(discipline)}>
            <li className="title">{discipline.name}</li>
            <li className="text">{discipline.where}</li>
            <li className="text">{discipline.date}</li>
            <li className="text">{discipline.description}</li>
          </ul>
        );
      })}
    </div>
  );
};

export default Education;
