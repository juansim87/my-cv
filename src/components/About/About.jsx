import React from "react";
import "./About.scss";

const About = ({ about }) => {
  return (
    <div className="about">
      <h2 className="title">Sobre mí</h2>
      <ul className="info">
        {about.map((entry) => {
          return <li className="text" key={JSON.stringify(entry)}>{entry.info}</li>;
        })}
      </ul>
    </div>
  );
};

export default About;
