import React from "react";
import "./More.scss";

const More = ({ languages, abilities, volunteer }) => {
  return (
    <div className="more">
      <div className="box">
        <h2 className="title">🗨️​Idiomas</h2><div className="languages">
        {languages.map((item) => {
          return (
            <ul className="info">
              <li className="title">{item.language}</li>
              <li className="text">Escrito: {item.wrlevel}</li>
              <li className="text">Hablado: {item.splevel}</li>
            </ul>
          );
        })}</div>
      </div>
      <div className="box">
        <h2 className="title">​⚙️​Soft Skills</h2>
        <div className="info skills">
          {abilities.map((item) => {
            return <h3 className="text">{item}</h3>;
          })}
        </div>
      </div>
      <div className="box">
        <h2 className="title">​​🙋‍♂️​Voluntariado</h2>
        {volunteer.map((item) => {
          return (
            <ul className="info">
              <li className="title">{item.name}</li>
              <li className="text">{item.where}</li>
              <li className="text">{item.description}</li>
            </ul>
          );
        })}
      </div>
    </div>
  );
};

export default More;
