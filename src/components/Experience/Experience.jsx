import React from "react";
import "./Experience.scss";

const Experience = ({ experience }) => {
  return (
    <div className="experience">
      {experience.map((job) => {
        return (
          <ul className="info" key={JSON.stringify(job)}>
            <li className="title">{job.name}</li>
            <li className="text">{job.where}</li>
            <li className="text">{job.date}</li>
            <li className="text">{job.description}</li>
          </ul>
        );
      })}
    </div>
  );
};

export default Experience;
